#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

import pyatspi

from pyatspitest.mousetracklistener import MouseTrackListener
from pyatspitest import main


class Listener(MouseTrackListener):
    def _print_node_loc(self, node):
        if not node:
            return []
        return self._print_node_loc(node.parent) + [(node.name, node.getRoleName())]

    def onMouseMoved(self, event, accessible):
        super().onMouseMoved(event, accessible)

        if not accessible:
            return
        print(self._print_node_loc(accessible))
        interfaces = pyatspi.listInterfaces(accessible)
        print("[%s] %s: %s" % (hash(accessible), accessible.name, ', '.join(interfaces)))


if __name__ == '__main__':
    main(Listener(app=None))
