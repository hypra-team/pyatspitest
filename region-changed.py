#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

import pyatspi

from pyatspitest.baselistener import BaseListener
from pyatspitest import main


class Listener(BaseListener):
    def __init__(self):
        super().__init__(types=[
            "screen-reader:region-changed",
        ])

    def _on_event(self, event):
        if super()._on_event(event):
            return True

        # ignore empty ranges
        if event.detail1 == event.detail2:
            return True

        try:
            text = event.source.queryText()
        except Exception as ex:
            print("Error querying text interface:", ex)
            return False

        rect = text.getRangeExtents(event.detail1, event.detail2, 0)
        substr = text.getText(event.detail1, event.detail2)
        print("text at %d:%d is [%s] and is on screen at %dx%s%+d%+d" %
              (event.detail1, event.detail2, substr, *rect[2:4], *rect[0:2]))

        return True


if __name__ == '__main__':
    main(Listener())
