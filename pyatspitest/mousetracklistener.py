#!/usr/bin/python3
# coding=utf-8 license=GPL-3.0+
# Inspired by Orca's mouse tracking

import time
import logging
import pyatspi
import gi
from gi.repository import GLib
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk
gi.require_version('Wnck', '3.0')
from gi.repository import Wnck

from .applistener import AppListener


class MouseTrackListener(AppListener):
    def __init__(self, app=None):
        super().__init__(types=[
            "mouse:abs",
        ], app=app)

        self._handlerIds = {}
        self._windows = []
        self._all_windows = []
        self._timeout_id = 0

    def __enter__(self):
        screen = Wnck.Screen.get_default()
        if screen:
            i = screen.connect("window-stacking-changed", self._on_stacking_changed)
            self._handlerIds[i] = screen
            i = screen.connect("active-workspace-changed", self._on_workspace_changed)
            self._handlerIds[i] = screen

        super().__enter__()

    def __exit__(self, *args, **kwargs):
        for key, value in self._handlerIds.items():
            value.disconnect(key)
        self._handlerIds = {}

        super().__exit__(*args, **kwargs)

    def _update_active_workspace(self, screen):
        ws = screen.get_active_workspace()
        if not ws:
            return
        self._windows = [w for w in self._all_windows if w.is_on_workspace(ws)]
        logging.debug("current WS windows are: %s", [w.get_name() for w in self._windows])

    def _on_stacking_changed(self, screen):
        """Callback for Wnck's window-stacking-changed signal."""
        logging.debug("window stacking changed")

        stacked = screen.get_windows_stacked()
        stacked.reverse()
        self._all_windows = stacked
        self._update_active_workspace(screen)

    def _on_workspace_changed(self, screen, prev_ws=None):
        """Callback for Wnck's active-workspace-changed signal."""
        logging.debug("workspace changed")

        self._update_active_workspace(screen)

    def _on_event(self, e):
        if super()._on_event(e):
            return True

        def onTimeout():
            self._handle_event(e)
            self._timeout_id = 0
            return False

        if self._timeout_id:
            GLib.source_remove(self._timeout_id)
        self._timeout_id = GLib.timeout_add(200, onTimeout)

        return True

    def _contains_point(self, obj, x, y, coordType=None):
        if coordType is None:
            coordType = pyatspi.DESKTOP_COORDS

        try:
            return obj.queryComponent().contains(x, y, coordType)
        except:
            return False

    def _has_bounds(self, obj, bounds, coordType=None):
        """Returns True if the bounding box of obj is bounds."""

        if coordType is None:
            coordType = pyatspi.DESKTOP_COORDS

        try:
            extents = obj.queryComponent().getExtents(coordType)
        except:
            return False

        return list(extents) == list(bounds)

    def _accessible_window_at_point(self, pX, pY):
        """Returns the accessible window at the specified coordinates."""

        window = None
        for w in self._windows:
            if w.is_minimized():
                continue

            x, y, width, height = w.get_geometry()
            if x <= pX <= x + width and y <= pY <= y + height:
                window = w
                break

        if not window:
            return None

        app = None
        windowApp = window.get_application()
        if not windowApp:
            return None

        pid = windowApp.get_pid()
        for a in pyatspi.Registry.getDesktop(0):
            if a.get_process_id() == pid:
                app = a
                break

        if not app:
            return None

        candidates = [o for o in app if self._contains_point(o, pX, pY)]
        if len(candidates) == 1:
            return candidates[0]

        name = window.get_name()
        matches = [o for o in candidates if o.name == name]
        if len(matches) == 1:
            return matches[0]

        bbox = window.get_client_window_geometry()
        matches = [o for o in candidates if self._has_bounds(o, bbox)]
        if len(matches) == 1:
            return matches[0]

        return None

    def _accessible_at_pointer(self):
        pX, pY = Gdk.Display.get_default().get_default_seat().get_pointer().get_position()[1:]

        window = self._accessible_window_at_point(pX, pY)
        logging.debug("window = %s", window)

        def recAccessibleAtPoint(obj):
            while obj:
                node = obj.queryComponent().getAccessibleAtPoint(pX, pY, pyatspi.DESKTOP_COORDS)
                if node is None and \
                   obj.getRole() == pyatspi.ROLE_INTERNAL_FRAME:
                    # unfortunately Firefox is not able to pass the internal
                    # frame barrier (e10s subprocess bridge), so we manually
                    # re-try children in this case
                    for child in obj:
                        node = recAccessibleAtPoint(child)
                        if node:
                            return node
                    return obj
                elif node is None and obj.getRole() == pyatspi.ROLE_PAGE_TAB_LIST:
                    # unfortunately GtkNotebook's PAGE_TABs do not include the
                    # bounds of their children.  Work around this.
                    try:
                        selection = obj.querySelection()
                    except:
                        pass
                    else:
                        # find the active page(s)
                        for i in range(0, selection.nSelectedChildren):
                            page = selection.getSelectedChild(i)
                            if page is None:
                                continue
                            # Check if a child of the page matches.  The page
                            # itself cannot match, otherwise it would have been
                            # found by the getAccessibleAtPoint() call above.
                            for child in page:
                                node = recAccessibleAtPoint(child)
                                if node:
                                    return node
                    return obj
                elif node is None or node == obj:
                    return obj
                obj = node
            return None

        return recAccessibleAtPoint(window)

    def _handle_event(self, e):
        try:
            dummy = e.source.getRole()
        except:
            return False

        logging.debug("--- %s ---\n%s", time.strftime('%X'), e)
        acc = self._accessible_at_pointer()
        self.onMouseMoved(e, acc)
        return True

    def onMouseMoved(self, event, accessible):
        """ Handler for mouse movement events.
        event: The AT-SPI2 event
        accessible: The accessible under the mouse, or None

        The default handler simply prints some debugging info tracking
        the element.  There is no need to chain up to it.
        """

        logging.debug("accessible under the mouse is %s", accessible)
