#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

import pyatspi

from pyatspitest.applistener import AppListener
from pyatspitest import main


class Listener(AppListener):
    def __init__(self, app=None):
        super().__init__(types=[
            "screen-reader",
        ], app=app)
    
    def _print_node_loc(self, node):
        if not node:
            return []
        return self._print_node_loc(node.parent) + [(node.name, node.getRoleName())]

    def _on_event(self, event):
        if super()._on_event(event):
            return True

        print(event)
        return True


if __name__ == '__main__':
    main(Listener(app=None))
