#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

import pyatspi


class BaseListener:
    def __init__(self, types):
        self.__types = types

    def __enter__(self):
        for t in self.__types:
            pyatspi.Registry.registerEventListener(self._on_event, t)
        pyatspi.Registry.start()

    def __exit__(self, *args, **kwargs):
        pyatspi.Registry.stop()
        for t in self.__types:
            pyatspi.Registry.deregisterEventListener(self._on_event, t)

    def _on_event(self, event):
        return False
