#!/usr/bin/env python3
# coding=utf-8 license=Apache-2.0

import pyatspi

import gi
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk


# from dogtail's rawinput
def keyNameToKeySym(keyName):
    keySym = Gdk.keyval_from_name(keyName)
    # various error 'codes' returned for non-recognized chars in versions of GTK3.X
    if keySym == 0xffffff or keySym == 0x0 or keySym is None:
        try:
            keySym = uniCharToKeySym(keyName)
        except:  # not even valid utf-8 char
            try:  # Last attempt run at a keyName ('Meta_L', 'Dash' ...)
                keySym = getattr(Gdk, 'KEY_' + keyName)
            except AttributeError:
                raise KeyError(keyName)
    return keySym


if __name__ == '__main__':
    from sys import argv

    for arg in argv[1:]:
        sym = keyNameToKeySym(arg)
        pyatspi.Registry.generateKeyboardEvent(sym, None, pyatspi.KEY_SYM)
