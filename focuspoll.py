#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

import pyatspi

from pyatspitest.baselistener import BaseListener
from pyatspitest import main


class Listener(BaseListener):
    def __init__(self):
        super().__init__(types=[
            "object:state-changed:focused",
            "object:text-caret-moved",
            "object:text-changed:inserted",
            "object:text-changed:removed",
            "object:state-changed:selected",
            "window:create",
            "object:active-descendant-changed",
            "screen-reader:region-changed",
        ])
    
    def _print_node_loc(self, node):
        if not node:
            return []
        return self._print_node_loc(node.parent) + [(node.name, node.getRoleName())]

    def _on_event(self, event):
        if super()._on_event(event):
            return True

        print(event)
        return True


if __name__ == '__main__':
    main(Listener())
