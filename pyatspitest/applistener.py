#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

from .baselistener import BaseListener


class AppListener(BaseListener):
    def __init__(self, types, app=None):
        super().__init__(types)
        self._app_name = app

    def _on_event(self, event):
        if super()._on_event(event):
            return True

        return self._app_name is not None and \
               self._app_name.casefold() != e.host_application.name.casefold()
