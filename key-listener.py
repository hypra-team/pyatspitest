#!/usr/bin/env python3
# coding=utf-8 license=Apache-2.0

import pyatspi

import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk, Gtk

from pyatspitest import main


class Listener:
    def __init__(self):
        self._listeners = []

    def _on_binding(self, keyval, modifiers, is_press):
        print("[%s] %s (key=%s mods=%s)" % (['P', 'R'][not is_press],
                                            Gtk.accelerator_name(keyval, Gdk.ModifierType(modifiers)),
                                            keyval, modifiers))

    def _event(self, event):
        print(event)
        return False
        
        modifiers = 0
        if event.modifiers & (1 << pyatspi.MODIFIER_ALT):
            modifiers |= Gdk.ModifierType.MOD1_MASK
        if event.modifiers & (1 << pyatspi.MODIFIER_CONTROL):
            modifiers |= Gdk.ModifierType.CONTROL_MASK
        # This is often NumLockMask, so strip it
        # ~ if event.modifiers & (1 << pyatspi.MODIFIER_META):
            # ~ modifiers |= Gdk.ModifierType.META_MASK
        if event.modifiers & (1 << pyatspi.MODIFIER_META2):
            modifiers |= Gdk.ModifierType.MOD2_MASK
        if event.modifiers & (1 << pyatspi.MODIFIER_META3):
            modifiers |= Gdk.ModifierType.MOD3_MASK
        if event.modifiers & (1 << pyatspi.MODIFIER_SHIFT):
            modifiers |= Gdk.ModifierType.SHIFT_MASK
        if event.modifiers & (1 << pyatspi.MODIFIER_SHIFTLOCK):
            modifiers |= Gdk.ModifierType.LOCK_MASK

        keyval = event.id
        # meh.
        if event.modifiers & (1 << pyatspi.MODIFIER_SHIFT) and Gdk.keyval_is_upper(keyval):
            keyval = Gdk.keyval_to_lower(keyval)
            modifiers |= Gdk.ModifierType.SHIFT_MASK

        modifiers &= Gtk.accelerator_get_default_mod_mask()

        self._on_binding(keyval, modifiers, event.type == pyatspi.KEY_PRESSED_EVENT)

        return False

    def __enter__(self):
        for i in range(256):
            self._listeners.append(pyatspi.Registry.registerKeystrokeListener(self._event, mask=i))
        pyatspi.Registry.start()

    def __exit__(self, *args, **kwargs):
        pyatspi.Registry.stop()
        for l in self._listeners:
            pyatspi.Registry.deregisterKeystrokeListener(l)
        self._listeners.clear()



if __name__ == '__main__':
    main(Listener())
