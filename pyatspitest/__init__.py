#!/usr/bin/python3
# coding=utf-8 license=Apache-2.0

from .baselistener import BaseListener
from .applistener import AppListener


def main(listener):
    try:
        with listener as dummy:
            pass
    except KeyboardInterrupt:
        pass
