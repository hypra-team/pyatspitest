#!/usr/bin/python3

import time
import sys
import pyatspi
from gi.repository import GLib

from .pyatspitest import AppListener, main


def getRelations(obj):
    """ Gets relations as a dict decoupled from the RelationSet """
    rels = {}
    for rel in obj.getRelationSet():
        rt = rel.getRelationType()
        if rt not in rels:
            rels[rt] = []
        rels[rt] += [rel.getTarget(i) for i in range(rel.getNTargets())]
    return rels

def getStates(obj):
    """ Gets the states as a list decoupled from the StateSet """
    return [s.value_nick for s in obj.getState().get_states()]

def diffList(list1, list2):
    """ Diffs two lists """
    return ['-%r' % i for i in list1 if i not in list2] + \
           ['+%r' % i for i in list2 if i not in list1]

def diffDict(dict1, dict2):
    """ Diffs two similar dicts.  Values must have the same type in both
    instances if they share the same key """
    diff = {}
    for k in dict1:
        if k not in dict2:
            diff['-%r' % k] = dict1[k]
        elif dict1[k] != dict2[k]:
            if isinstance(dict1[k], dict):
                diff[k] = diffDict(dict1[k], dict2[k])
            elif isinstance(dict1[k], list):
                diff[k] = diffList(dict1[k], dict2[k])
            else:
                diff[k] = '-%r +%r' % (dict1[k], dict2[k])
    for k in dict2:
        if k not in dict1:
            diff['+%r' % k] = dict2[k]
    return diff


class CacheCheckListener(AppListener):
    def __init__(self, app=None):
        super().__init__(types=[
            "object:state-changed:focused",
        ], app=app)

    def _on_event(self, e):
        if super()._on_event(e):
            return True

        if e.type != "object:state-changed:focused" or e.detail1 != 1:
            return False

        GLib.timeout_add(200, lambda: self._handle_event(e) and False)

        return True

    def diveDownChecking(self, obj):
        cached_state = getStates(obj)
        cached_children = [c for c in obj]
        cached_relations = getRelations(obj)
        obj.clearCache()
        state = getStates(obj)
        children = [c for c in obj]
        relations = getRelations(obj)
        if cached_state != state:
            print("*** cache issue on %s: state changed: %s"
                  % (obj, diffList(cached_state, state)))
        if cached_children != children:
            print("*** cache issue on %s: children changed: %s"
                  % (obj, diffList(cached_children, children)))
        if cached_relations != relations:
            print("*** cache issue on %s: relations changed: %s"
                  % (obj, diffDict(cached_relations, relations)))
        for child in obj:
            self.diveDownChecking(child)

    def _handle_event(self, e):
        if e.type != "object:state-changed:focused" or e.detail1 != 1:
            return False

        try:
            role = e.source.getRole()
        except:
            return False

        if role == pyatspi.ROLE_FRAME:
            frame = e.source
        else:
            frame = pyatspi.findAncestor(e.source, lambda x: x.getRole() == pyatspi.ROLE_FRAME)
        if not frame:
            return False

        print("--- %s ---\n%s" % (time.strftime('%X'), e))
        self.diveDownChecking(frame)
        return True


if __name__ == '__main__':
    main(CacheCheckListener(app=(sys.argv[1] or 'firefox')))
